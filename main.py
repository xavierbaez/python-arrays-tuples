import pandas as pd

global numbers_tuple
global cars_list
global numbers_list


numbers_tuple = (1, 2, 3, 0, 8, 9, 7, 6, 4, 5)
numbers_list = [1, 2, 3, 0, 8, 9, 7, 6, 4, 5]


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


if __name__ == '__main__':
    print_hi('PyCharm')


def list_example(numbers):
    print(list_example.__name__)
    print(numbers)


def tuple_example(numbers):
    print(tuple_example.__name__)
    print(numbers)


def sort_data(numbers):
    print(sort_data.__name__)
    numbers.sort()
    print(numbers)


def filter_even(numbers):
    print(filter_even.__name__)
    # result = [x for x in numbers if x % 2 == 0]
    result = []
    for x in numbers:
        if x % 2 == 0:
            result.append(x)
    print('The even numbers are')
    print(result)


def grab_range(numbers):
    print(grab_range.__name__)
    print('New numbers:', numbers[3:6])


def remove_last_element():
    print(remove_last_element.__name__)
    numbers_last_element = [1, 2, 3, 0, 8, 9, 7, 6, 4, 5]
    numbers_last_element.sort()
    del numbers_last_element[-1]
    print('List with removed last element', numbers_last_element)


def remove_second_element():
    print(remove_second_element.__name__)
    numbers_second_element = [1, 2, 3, 0, 8, 9, 7, 6, 4, 5]
    numbers_second_element.sort()
    del numbers_second_element[1]
    print('List with removed second element', numbers_second_element)


def remove_element():
    print(remove_element.__name__)
    numbers_remove_element = [1, 2, 3, 0, 8, 9, 7, 6, 4, 5]
    numbers_remove_element.sort()
    numbers_remove_element.remove(7)
    print('List with removed element(7): ', numbers_remove_element)


def filter_pandas():
    print(filter_pandas.__name__)
    df = pd.read_csv('nycflights.csv', usecols=range(1, 17))
    print(df)


tuple_example(numbers_tuple)
list_example(numbers_list)
sort_data(numbers_list)
filter_even(numbers_list)
grab_range(numbers_list)
remove_last_element()
remove_second_element()
remove_element()
filter_pandas()
